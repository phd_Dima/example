'use strict'

application_factory = ($resource) ->
  $resource '', { format: 'json' },
    search: { method: 'POST', url: '/search' }

audio_factory = ($resource) ->
  root_url = '/audios/:id'
  $resource root_url, { id: '@id' },
    show: { }

audio_category_factory = ($resource) ->
  root_url = '/audio_categories/:id'
  $resource root_url, { id: '@id' },
    query: { isArray: true }

category_factory = ($resource) ->
  root_url = '/categories/:id'
  $resource root_url, { id: '@id', format: 'json' },
    query: { isArray: true }

packshots_factory = ($resource) ->
  root_url = '/posts/:post_id/packshots/:id'
  $resource root_url, { post_id: '@post_id', id: '@id', format: 'json' },
    query:        { url: '/packshots', isArray: true }
    create:       { method: 'POST' }
    save:         { method: 'PUT' }
    edit:         { method: 'GET', url: '/packshots/:id/edit' }
    destroy:      { method: 'DELETE', url: '/packshots/:id' }
    in_library:   { method: 'POST', url: '/packshots/:id/in_library' }
    cancel:       { method: 'DELETE', url: '/packshots/:id' }

posts_factory = ($resource) ->
  root_url = '/posts/:id'
  $resource root_url, { id: '@id', format: 'json' },
    query: { isArray: true }
    wishlist: { url: '/profile/wishlists', isArray: true }
    new: { url: root_url + '/new' }
    edit: { url: root_url + '/edit' }
    save: { method: 'PUT' }
    show: { }
    preview: { url: root_url + '/preview' }
    cancel: { method: 'DELETE', url: root_url + '/cancel' }
    concern_like: { method: 'POST', url: root_url + '/likes' }
    addToWishlist:      { method: 'POST', url: root_url + '/wishlists' }
    removeFromWishlist: { method: 'DELETE', url: root_url + '/wishlists/self' }
    destroy: { method: 'DELETE' }
    build_hls_url: { url: root_url  + '/build_hls_url' }
    update_packshot: { method: 'POST', url: root_url  + '/update_packshot' }

comment_factory = ($resource) ->
  root_url = '/comments/:id'
  $resource root_url, { id: '@id', format: 'json' },
    index: { method: 'GET', isArray: true }
    create: { method: 'POST' }
    concern_like: { method: 'POST', url: root_url + '/likes', params: { like: { likable: 'comment' } } }

music_factory = ($resource) ->
  root_url = '/music'
  $resource root_url, { id: '@id', format: 'json' },
    query: { method: 'POST', isArray: true }

download_factory = ($resource) ->
  root_url = '/downloads'
  $resource root_url, { id: '@id', format: 'json' },
    create: { method: 'POST' }

transaction_factory = ($resource) ->
  root_url = '/transactions/:id'
  $resource root_url, { id: '@id', format: 'json' },
    query: { method: 'GET', url: '/profile' + root_url, isArray: true }
    save: { method: 'PUT' }

user_factory = ($resource) ->
  root_url = '/profile/:id'
  $resource root_url, { id: '@id', format: 'json' },
    show: { method: 'GET' }
    update: { url: root_url + '/information', method: 'PUT' }
    update_password: { url: '/users/password', method: 'PUT' }
    upload_photo: { method: 'POST', url: root_url + '/photo' }
    packshot: { url: root_url + '/packshot' }
    create_packshot: { method: 'POST', url: '/packshots' }
    update_packshot: { method: 'PUT', url: '/packshots/:id' }
    following: { method: 'POST', url: '/users/:id/following' }

notification_factory = ($resource) ->
  root_url = '/notifications/:id'
  $resource root_url, { id: '@id', format: 'json' },
    query: { method: 'GET' }

@app
.factory 'Application', ['$resource', application_factory]
.factory 'Audio', ['$resource', audio_factory]
.factory 'AudioCategory', ['$resource', audio_category_factory]
.factory 'Category', ['$resource', category_factory]
.factory 'Packshot', ['$resource', packshots_factory]
.factory 'Post', ['$resource', posts_factory]
.factory 'Comment', ['$resource', comment_factory]
.factory 'Music', ['$resource', music_factory]
.factory 'Download', ['$resource', download_factory]
.factory 'Transaction', ['$resource', transaction_factory]
.factory 'User', ['$resource', user_factory]
.factory 'Notification', ['$resource', notification_factory]
