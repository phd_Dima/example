var gulp = require("gulp");
var del = require("del");
var path = require("path");
var lazypipe = require("lazypipe");
var $ = require("gulp-load-plugins")({lazy: false});

var paths = {
    html: {
        src: "./src",
        tmp: "./build/tmp/html"
    },
    js: {
        libs: "libs",
        src: "./assets/js",
        tmp: "./build/tmp/static/js",
        target: "./build/static/js"
    },
    scss: {
        src: "./scss",
        tmp: "./build/tmp/static/css/scss/",
        target: "./build/static/css/scss/"
    },
    assets: {
        src: "./assets",
        tmp: "./build/tmp/static",
        target: "./build/static/sources/assets"
    },
    target: {
        root: "./build",
        static: "./build/static",
        js: "./build/static/js",
        css: "./build/static/css",
        nginx: "./build/nginx/conf"
    },
    nginx: {
        root: "./other/nginx/",
        target: "./build/nginx/",
        conf: {
            dev: "./other/nginx/dev/",
            prod: "./other/nginx/prod/",
            shared: "./other/nginx/shared/",
            target: "./build/nginx/conf"
        }
    }
};

var _ = {
    vars: {},
    pathAny: function (dir, extension) {
        return path.join(dir, "**/*." + (extension || "*"))
    },
    pathAnyExcept: function(dir, extension, except) {
        return [_.pathAny(dir, extension), _.pathExcept(_.pathAny(path.join(dir, except)))]
    },
    pathExcept: function(path) {
        return "!" + path;
    },
    isProduction: function () {
        return !!_.vars.prod;
    },
    ifProduction: function (doStream) {
        return _.isProduction() ? doStream : gutil.noop();
    },
    isEnv: function (variable) {
        return !($.util.env[variable] === undefined)
    },
    getEnv: function (variable, defaultValue) {
        var answer = process.env[variable];

        if (answer === undefined) {
            return defaultValue;
        } else {
            return answer;
        }
    }
};

gulp.task("clean:static", function (cb) {
    del(paths.target.static, cb);
});

(function assets() {
    gulp.task("clean:static.assets.tmp", function (cb) {
        del(paths.assets.tmp, cb);
    });

    gulp.task("clean:static.assets.build", function (cb) {
        del([paths.target.js, paths.target.css], cb);
    });

    gulp.task("clean:static.assets", gulp.parallel("clean:static.assets.tmp", "clean:static.assets.build"));

    gulp.task("assets.scss", function () {
        return gulp.src(_.pathAny(paths.scss.src))
            .pipe($.sourcemaps.init())
            .pipe($.sass({
                includePaths: [paths.scss.src]
            }))
            .pipe($.autoprefixer("last 1 version", "> 1%", "ie 8"))
            .pipe($.sourcemaps.write())
            .pipe(gulp.dest(paths.scss.tmp));
    });

    gulp.task("assets.scss:clean", function (cb) {
        del([paths.scss.tmp, paths.scss.target], cb);
    });

    gulp.task("assets.scss:tmp-to-build", function () {
        return gulp.src(_.pathAny(paths.scss.tmp)).pipe(gulp.dest(paths.scss.target))
    });

    gulp.task("assets:src-to-tmp", function() {
        return gulp
            .src([_.pathAny(paths.assets.src), _.pathExcept(paths.js.src)])
            .pipe(gulp.dest(paths.assets.tmp))
    });

    gulp.task("assets:js.libs", function() {
        return gulp
            .src(_.pathAny(path.join(paths.js.src, paths.js.libs)))
            .pipe(gulp.dest(path.join(paths.js.tmp, paths.js.libs)));
    });

    gulp.task("assets:js.compile", function() {
        return gulp
            .src(_.pathAnyExcept(paths.js.src, "js", paths.js.libs))
            .pipe($.sourcemaps.init())
            .pipe($.babel({
                    presets: ['es2015']
                }))
            .pipe($.sourcemaps.write("."))
            .pipe(gulp.dest(paths.js.tmp))
    });

    gulp.task("assets:tmp-to-build", function() {
        return gulp
            .src(_.pathAny(paths.assets.tmp))
            .pipe(gulp.dest(paths.target.static))
    });

    gulp.task("assets", gulp.series("assets:src-to-tmp", "assets.scss", "assets:js.compile", "assets:js.libs", "assets:tmp-to-build"));
})();

(function html() {
    gulp.task("html:preprocess", function() {
        var context = {
            environment: (_.isProduction()) ? "prod" : "dev"
        };

        return gulp
            .src(_.pathAny(paths.html.src, "html"))
            .pipe($.preprocess({context: context}))
            .pipe(gulp.dest(paths.html.tmp))
    });

    gulp.task("html:extend", function() {
        return gulp
            .src(_.pathAny(paths.html.tmp, "html"))
            .pipe($.htmlExtend({annotations: false, verbose: true, root: paths.html.tmp}))
            .pipe(gulp.dest(paths.target.static))
    });

    gulp.task("html:clean-tmp", function(cb) {
        del(paths.html.tmp, cb);
    });

    gulp.task("html", gulp.series("html:preprocess", "html:extend", "html:clean-tmp"));
})();

gulp.task("build.dev", gulp.series("clean:static", "html", "assets"));

gulp.task("build.prod", gulp.series("clean:static", "html", "assets"));

(function nginx() {
    var processNginxConfig = lazypipe()
        .pipe($.replace, "$MAYA_NGINX_ROOT", _.getEnv("MAYA_NGINX_ROOT", "/job"));

    gulp.task("nginx:clean", function (cb) {
        del(paths.nginx.conf.target, cb);
    });

    gulp.task("nginx:dev.conf", function () {
        return gulp
            .src(_.pathAny(paths.nginx.conf.dev))
            .pipe(processNginxConfig())
            .pipe(gulp.dest(paths.nginx.conf.target));
    });

    gulp.task("nginx:dev.log", function () {
        return gulp
            .src(_.pathAny(path.join(paths.nginx.root, "log")))
            .pipe(gulp.dest(path.join(paths.nginx.target, "log")));
    });

    gulp.task("nginx:prod.conf", function () {
        return gulp
            .src(_.pathAny(paths.nginx.conf.prod))
            .pipe(processNginxConfig())
            .pipe(gulp.dest(paths.nginx.conf.target));
    });

    gulp.task("nginx:shared.conf", function () {
        return gulp
            .src(_.pathAny(paths.nginx.conf.shared))
            .pipe(processNginxConfig())
            .pipe(gulp.dest(paths.nginx.conf.target));
    });

    gulp.task("nginx.dev", gulp.series("nginx:clean", gulp.parallel("nginx:dev.conf", "nginx:shared.conf", "nginx:dev.log")));

    gulp.task("nginx.prod", gulp.series("nginx:clean", gulp.parallel("nginx:prod.conf", "nginx:shared.conf")));
}());

(function watch() {
    gulp.task("watch:assets", function() {
        $.watch([_.pathAny(paths.assets.src)], gulp.series("clean:static.assets", "assets"))
    });

    gulp.task("watch:assets.scss", function() {
        $.watch(_.pathAny(paths.scss.src), gulp.series("assets.scss:clean", "assets.scss", "assets.scss:tmp-to-build"))
    });

    gulp.task("watch:html", function() {
        $.watch(_.pathAny(paths.html.src), gulp.series("build.dev"))
    });

    gulp.task("watch:nginx", function() {
        $.watch(_.pathAny(paths.nginx.root), gulp.series("nginx.dev"))
    });

    gulp.task("watch", gulp.series("build.dev", "nginx.dev", gulp.parallel("watch:assets", "watch:html", "watch:nginx", "watch:assets.scss")));
})();