@app.directive 'postDetails', ->
  restrict: 'E'
  transclude: true
  replace: true
  scope:
    post: '='
  templateUrl: @template 'directives/posts/details'
  link: ($scope, element, attrs) ->
  controller: [
    '$scope', '$timeout', 'Post'
    ($scope, $timeout, Post) ->

      read_more = ->
        article = angular.element '.details article'
        h = angular.element '.details article h'
        p = angular.element '.details article p'
        $scope.read_more = article.height() < (h.height() + p.height())

      $timeout read_more, 2000

      $scope.like = ->
        @post.is_liked = !@post.is_liked
        Post.concern_like({ id: @post.id }).$promise.then(
          (response) =>
            @post.is_liked = response.is_liked
            @post.likes_count = response.likes_count
        )
  ]