@app.directive 'feedList', ->
  restrict: 'E'
  transclude: true
  replace: true
  scope:
    feeds: '='
    reply: '='
    user:  '='
    profile: '='
    market: '='
    infinite: '='
  templateUrl: @template 'directives/posts/feed_list'
  controller: [
    '$scope', '$window', '$timeout'
    ($scope, $window, $timeout) ->
      $scope.columns =
        calc: ->
          if @count isnt @getCount()
            @count = @getCount()
            @data = [0...@count]
            $scope.columnClass = "size-1of#{@count}"

        getCount: ->
          count = window.getComputedStyle(angular.element('.feeds-content')[0],':before').content.charAt(1) * 1 #"3
          if typeof count is 'number' then count else 1

        recalc: ->
          $timeout.cancel(@timeout_promise) if @timeout_promise
          @timeout_promise = $timeout @calc(), 250

      $scope.onRemove = (id) ->
        for feed, index in @feeds
          if feed.id == id
            @feeds.splice(index, 1)
            break

      $scope.columns.calc()

      angular.element($window).resize ->
        $scope.columns.recalc()

  ]