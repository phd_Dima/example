@app.directive 'relevantList', ->
  restrict: 'E'
  transclude: true
  replace: true
  scope:
    relevants: '='
  templateUrl: @template 'directives/posts/relevant_list'
  controller: [
    '$scope'
    ($scope) ->
  ]