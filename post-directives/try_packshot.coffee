'use strict'

tryPackshotDirective = ($scope, $timeout, ws, packshot, User, Post, Packshot) ->
  vm = this

  callbacks =
    save_success: (d) ->
      [ui.is_ready, packshot.changed] = [true, false]
      (packshot.models.upload || {}).image = null
      packshot.edit.init packshot.models.generate
    in_library_success: (d) -> ui.is_ready = true
    build_hls_url_success: (data) ->
      [ui.state, ui.preview_kind, ui.is_ready, vjs.media.sources[0].src] = ['preview', 'video', true, data.url]
      if vm.demo
        $timeout (-> jwp.init(data.url)), 250
      else
        $timeout vjs.play, 250 #quick crutch
    error: (d) -> [ui.is_ready, data.errors] = [true, d.data.errors]

  data =
    user: vm.user
    packshots: []

  vjs =
    is_ready: false
    options:
      loop: false
      muted: false
      preload: 'none'
      autoplay: true
      poster: null
      controls: true
    media:
      sources: [{ src: null, type: 'application/x-mpegURL' }]
      tracks: []
    play: -> vjs.is_ready = true
    stop: ->
      return unless vjs.player
      vjs.player.dispose()
      vjs.player = null

  ws_callbacks =
    onOpen: -> console.log 'ws.onOpen'
    onMessage: (message) ->
      switch message.kind
        when 'packshots:video:ready' then packshot.ws_message message
        else console.log 'Unforeseen message kind', message.kind
    onClose: -> console.log 'ws.onClose'

  save = ->
    [ui.is_ready, packshot.changed, data.errors] = [false, false, null]
    args =
      packshot:
        image: false
        brand_name: packshot.edit.name
        slogan: packshot.edit.slogan
    action = User.create_packshot
    if (packshot.models.generate || {}).id
      action = User.update_packshot
      args.id = packshot.models.generate.id
    packshot.models.generate = action args, callbacks.save_success, callbacks.error

  query_packshots = ->
    return unless ui.is_ready
    ui.is_ready = false
    data.packshots = Packshot.query {}, ->
      [ui.is_ready, ui.can_choose] = [true, data.packshots.length]
      $scope.$broadcast 'select:init'
  ui =
    is_ready: true
    state: 'create'
    preview_kind: 'video'
    can_choose: false
    init: ->
      return unless ui.is_ready
      ui.is_ready = false
      User.packshot {}, (d) ->
        ui.upload.params.upload_path = d.ui.upload_image_path
        packshot.models.generate = d
        packshot.models.upload = d
        packshot.edit.init packshot.models.generate
        ui.is_ready = true
    onChange: -> packshot.changed = true
    onSave: -> packshot.changed && save()
    canPreview: -> ui.is_ready && packshot.canPreview()
    canUpload: ->
      return false if ui.upload.percent || packshot.edit.existsImage()
      !packshot.models.chosen
    onPreview: (args) ->
      args = {} unless args && typeof args is 'object'
      return unless ui.is_ready
      packshot_id = args.packshot_id
      unless packshot_id
        packshot_id = if packshot.canPreview() then packshot.models.current().id else null
      return unless packshot_id
      ui.is_ready = false
      Post.build_hls_url { id: vm.postId, packshot_id: packshot_id }, callbacks.build_hls_url_success, callbacks.error
    onEdit: -> ui.state = 'create'
    onBuyNow: -> window.location.replace "#{location.origin}/transactions/new?post_id=#{vm.postId}&packshot_id=#{packshot.models.current().id}" if ui.is_ready
    onCreateKind: (kind) ->
      packshot.state = kind
      if packshot.state is 'upload'
        query_packshots()
      else
        ui.toUpload()
    onInLibrary: ->
      return unless ui.is_ready
      ui.is_ready = false
      packshot.models.upload = Packshot.in_library { id: packshot.models.upload.id }, callbacks.in_library_success, callbacks.error
    toUpload: -> packshot.models.chosen = null
    upload:
      percent: 0
      params:
        method: 'PUT'
        upload_path: null
        allow_file_types: ['image/jpeg', 'image/gif', 'image/png', 'image/tiff']
        request_args:
          'packshot[brand_name]': ''
          'packshot[slogan]': ''
      onBefore: ($data, $callback) ->
        [ui.is_ready, data.errors] = [false, null]
#        $data = { duration: 0 } unless $data && typeof $data is 'object'
#        if $data.duration < 5 || $data.duration > 31
#          $scope.errors = { base: ["Error: Your video have incorrect duration, #{$data.duration} instead 30 seconds! Correct one please and try again."] }
#          return
        packshot.edit.reset()
        $callback()
      onProcess: ($percent_complete) -> ui.upload.percent = $percent_complete
      onSuccess: ($data) -> [ui.is_ready, ui.upload.percent, packshot.models.upload] = [true, 0, $data]
      onError: ($errors) -> data.errors = $errors
    onRemoveAttachment: packshot.edit.removeAttachment
    onCancel: ->
      return unless ui.is_ready
      ui.is_ready = false
      Packshot.cancel { id: packshot.models.generate.id }, ->
        packshot.edit.reset()
        [ui.state, packshot.state, ui.preview_kind, ui.is_ready] = ['create', 'generate', 'video', true]
        vm.onCancel()
        ui.init()
    onExample: (packshot_id) -> @onPreview { packshot_id: packshot_id }

  ws.init { user_id: data.user.id }, ws_callbacks
  ui.init()

  $scope.$on 'vjsVideoReady', (e, data) ->
    return unless ui.state is 'preview'
    vjs.player = data.player
    vjs.player.removeChild 'BigPlayButton'
    vjs.player.on 'pause', ->
      if vjs.player.ended() || (vjs.player.duration() - vjs.player.currentTime()) < 1
        ui.preview_kind = 'examples'
        vjs.stop()
        vm.apply $scope

  jwp =
    player: null
    played: false
    options:
      height: 620
      width: 1104
      primary: 'html5'
      autostart: true
    init: (hls_url) ->
      _packshot = packshot.models.current()
      @options.file = hls_url
      @options.image = _packshot.video.poster
      @player = jwplayer 'video-player-try-packshot'
      @player.setup @options
      @player.on 'complete', jwp.stop
    play: (on_off) ->
      @played = if on_off is undefined then !@played else on_off
      @player.play @played
    stop: ->
      @player.remove()
      ui.preview_kind = 'examples'
      vm.apply $scope

  vm.service = packshot
  vm.edit = packshot.edit
  vm.data = data
  vm.vjs = vjs
  vm.ui = ui
  vm


tryPackshotDirective.prototype = Object.create @BaseDirective.prototype

angular.module('RootApp').directive 'tryPackshot', ->
  restrict: 'E'
  scope:
    user: '='
    postId: '='
    demo: '='
    onCancel: '&'
  controllerAs: 'vm'
  bindToController: true
  templateUrl: 'directives/posts/try_packshot.html'
  controller: ['$scope', '$timeout', 'ws', 'packshot', 'User', 'Post', 'Packshot', tryPackshotDirective]