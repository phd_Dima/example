'use strict'

postsPreviewStep = ($scope, packshot, Post) ->
  vm = this

  callbacks =
    build_hls_url_success: (data) -> [ui.is_ready, vjs.media.sources[0].src] = [true, data.url]; vjs.play()
    error: (d) -> [ui.is_ready, vm.errors] = [true, d.data.errors]

  vjs =
    is_ready: false
    paused: true
    init: ->
      vjs.pause()
      vjs.is_ready = ui.canPreview()
      post = vm.post || {}
      video_ui = (post.video || { }).ui
      if video_ui && video_ui.hls_ready
        vjs.options.poster = vm.post.video.poster
        vjs.media.sources[0].src = vm.hls_url vm.post
    options:
      loop: false
      muted: false
      preload: 'none'
      autoplay: true
      poster: null
      controls: true
    media:
      sources: [{ src: null, type: 'application/x-mpegURL' }]
      tracks: []
    play: -> vjs.paused = false
    pause: ->
      if vjs.player
        vjs.player.dispose()
        vjs.player = null
      vjs.paused = true

  ui =
    collapsed: false
    is_ready: true
    onPlay: ->
      return unless ui.is_ready
      ui.is_ready = false
      packshot_id = if packshot.canPreview() then packshot.models.current().id else false
      Post.build_hls_url { id: vm.post.id, packshot_id: packshot_id }, callbacks.build_hls_url_success, callbacks.error
    isAvailable: ->
      return false unless (vm.post || {}).video
      vm.post.video.file_name
    onCollapse: ->
      ui.collapsed = !ui.collapsed
      vjs.pause() if ui.collapsed
    canPreview: ->
      return false unless (vm.post || {}).id
      video_ui = (vm.post.video || { ui: {} }).ui || {}
      video_ui.hls_ready
    canPlay: ->
      return false if !vm.post || vm.post.preview_job
      ui.is_ready && vjs.is_ready && vjs.paused

  $scope.$watch 'vm.post', (o, n) ->
    return unless vm.post
    if vm.post.$promise
      vm.post.$promise.then vjs.init
    else
      vjs.init()

  $scope.$watch 'vm.post.packshot', vjs.init

  $scope.$on 'vjsVideoReady', (e, data) ->
    vjs.player = data.player
    vjs.player.removeChild 'BigPlayButton'
    vjs.player.on 'ended', -> vjs.pause(); vm.apply $scope
    vjs.player.on 'pause', ->
      if vjs.player.ended() || (vjs.player.duration() - vjs.player.currentTime()) < 1
        vjs.pause()
        vm.apply $scope

  $scope.$on 'posts:video:status', (e, data) -> (vm.post || {}).preview_job = data

  $scope.$on 'posts:preview', vjs.init

  vjs.init()

  vm.vjs = vjs
  vm.ui = ui
  vm.job = {}
  vm


postsPreviewStep.prototype = Object.create @BaseDirective.prototype

angular.module('RootApp').directive 'postsPreviewStep', ->
  restrict: 'E'
  scope:
    post: '='
  controllerAs: 'vm'
  bindToController: true
  templateUrl: (elem, attrs) ->
    if attrs.creative
      'directives/posts/form/creative_preview_step.html'
    else
      'directives/posts/form/creative_preview_step.html'
  controller: ['$scope', 'packshot', 'Post', postsPreviewStep]