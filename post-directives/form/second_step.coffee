postsFormSecondStep = ($scope, $timeout, packshot, User, Post, Packshot) ->
  $scope.errors = null

  vjs =
    options:
      loop: false
      muted: true
      preload: 'metadata'
      autoplay: true
      poster: null
      width: 580
      height: 320
      controls: true
    media:
      sources: [{ src: null, type: 'video/mp4' }]
      tracks: []

  callbacks =
    in_library_success: (d) -> ui.is_ready = true
    save_success: (d) ->
      [ui.is_ready, ui.saved, packshot.changed] = [true, true, false]
      ui.upload.params.upload_path = packshot.models.generate.ui.upload_image_path
      packshot.edit.init packshot.models.generate
      notify 'posts:preview'
    error: (d) -> [$scope.ui.is_ready, $scope.errors] = [true, d.data.errors]

  data =
    packshots: []
    chosen_packshot: null

  query_packshots = ->
    return unless ui.is_ready
    ui.is_ready = false
    data.packshots = Packshot.query {}, ->
      [ui.is_ready, ui.can_choose] = [true, data.packshots.length]
      $scope.$broadcast 'select:init'

  refreshPost = ->
    $scope.post.packshot = packshot.models.current()
    $scope.refresh $post: $scope.post

  save = (callback) ->
    return unless ui.is_ready
    [ui.is_ready, packshot.changed, $scope.errors] = [false, false, null]
    args =
      post_id: $scope.post.id
      packshot:
        image: false
        brand_name: packshot.edit.name
        slogan: packshot.edit.slogan
    action = Packshot.create
    if (packshot.models.generate || {}).id
      args.id = packshot.models.generate.id
      action = Packshot.save
    success_h = (d) ->
      callback(d) if typeof callback is 'function'
      callbacks.save_success(d)
    packshot.models.generate = action args, success_h, callbacks.error

  notify = (kind) ->
    switch kind
      when 'posts:preview' then $scope.$emit 'packshots:video:ready', packshot.models.current()
      else throw new Error("postsFormSecondStep directive: unforeseen kind: #{kind}")

  ui =
    saved: false
    is_ready: true
    kind: 'edit'
    collapsed: false
    can_choose: false
    init: (d) ->
      $scope.post.packshot = d if (d || {}).id
      unless $scope.post.packshot
        return save ui.init
      _packshot = $scope.post.packshot
      return unless (_packshot || {}).id
      ui.upload.params.upload_path = _packshot.ui.upload_image_path
      packshot.models.generate = _packshot
      packshot.models.upload = _packshot
      packshot.edit.init _packshot
      @onCreateKind packshot.initState()
    isEditKind: -> ui.kind is 'edit'
    isPreviewKind: -> ui.kind is 'preview'
    onChange: -> [ui.saved, packshot.changed] = [false, true]
    onPreview: ->
      return unless ui.is_ready && packshot.canPreview()
      video = (packshot.models.current() || {}).video || {}
      vjs.media.sources[0].src = video.video_url || video.url
      ui.kind = 'preview'
    onEdit: -> ui.kind = 'edit'
    onSave: -> packshot.changed && save()
    onScroll: ->
      step_preview_offset = angular.element('.step-preview').offset()
      step_preview_target = step_preview_offset.top
      angular.element('html, body').animate { scrollTop: step_preview_target }, 300
      true
    onCreateKind: (kind) ->
      return unless kind
      packshot.state = kind
      if packshot.state is 'upload'
        query_packshots()
      else
        ui.toUpload()
      notify 'posts:preview'
    canPreview: -> ui.is_ready && packshot.canPreview()
    onPackshot: refreshPost
    canUpload: ->
      return false if ui.upload.percent || packshot.edit.existsImage()
      !packshot.models.chosen
    onInLibrary: ->
      return unless ui.is_ready
      ui.is_ready = false
      packshot.models.upload = Packshot.in_library { id: packshot.models.upload.id }, callbacks.in_library_success, callbacks.error
    toUpload: -> packshot.models.chosen = null
    upload:
      percent: 0
      params:
        method: 'PUT'
        upload_path: null
        allow_file_types: ['image/jpeg', 'image/gif', 'image/png', 'image/tiff']
        request_args:
          'packshot[brand_name]': ''
          'packshot[slogan]': ''
      onBefore: ($data, $callback) ->
        [ui.is_ready, $scope.errors] = [false, null]
        notify 'posts:preview'
        #        $data = { duration: 0 } unless $data && typeof $data is 'object'
        #        if $data.duration < 5 || $data.duration > 31
        #          $scope.errors = { base: ["Error: Your video have incorrect duration, #{$data.duration} instead 30 seconds! Correct one please and try again."] }
        #          return
        $callback()
      onProcess: ($percent_complete) -> ui.upload.percent = $percent_complete
      onSuccess: ($data) ->
        [ui.is_ready, ui.upload.percent, packshot.models.upload] = [true, 0, $data]
        packshot.edit.init $data
      onError: ($errors) -> $scope.errors = $errors
    onRemoveAttachment: packshot.edit.removeAttachment

  $scope.$on 'posts:cancel', ->
    packshot.edit.reset()
    packshot.models.generate = null
    packshot.models.upload = null
    packshot.models.chosen = null
    ui.kind = 'edit'

  $scope.$on 'packshots:video:ready', (e, new_packshot) ->
    return unless new_packshot
    packshot.ws_message kind: 'packshots:video:ready', packshot: new_packshot

  $scope.$watch 'post', (o, n) ->
    if $scope.post.$promise
      $scope.post.$promise.then -> ui.init()
    else
      ui.init()

  $scope.service = packshot
  $scope.vjs = vjs
  $scope.data = data
  $scope.ui = ui


angular.module('RootApp').directive 'postsFormSecondStep', ->
  restrict: 'E'
  transclude: true
  replace: true
  scope:
    post: '='
    data: '='
    refresh: '&'
  templateUrl: @template 'directives/posts/form/second_step'
  controller: ['$scope', '$timeout', 'packshot', 'User', 'Post', 'Packshot', postsFormSecondStep]