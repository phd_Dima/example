@app.directive 'postsFormThirdStep', ->
  restrict: 'E'
  transclude: true
  replace: true
  scope:
    post: '='
    data: '='
    creative: '='
    refresh: '&'
  templateUrl: (elem, attrs) =>
    if attrs.creative
      @template 'directives/posts/form/creative_third_step'
    else
      @template 'directives/posts/form/third_step'
  controller: ['$scope', '$timeout', 'Post', 'Category', 'Packshot', ($scope, $timeout, Post, Category, Packshot) ->
      callbacks =
        error: (d) -> $scope.data.errors = d.data.errors
        save_sucсess: (d) ->
          $scope.ui.is_ready = true
          if data.queue
            save()
          else
            [$scope.ui.changed, $scope.ui.saved] = [false, true]
            $scope.refresh { $post: d }; $scope.ui.init()

      data =
        timeout: null
        queue: null
        edit: {}

      init_save_timeout = ->
        if data.timeout
          $timeout.cancel data.timeout
          data.timeout = null
        data.timeout = $timeout (-> data.timeout = null; save()), 250

      save = ->
        return data.queue = true unless $scope.ui.is_ready
        data.queue = false
        $scope.ui.is_ready = false
        args =
          id: $scope.post.id
          post:
            price: $scope.form.price
            description: $scope.form.description
            tags: []
        args.post.category_id = $scope.form.category.id if ($scope.form.category || {}).id
        for tag in $scope.form.tags
          name = tag.name
          name = name.substr(1) if name.charAt(0) is '#'
          args.post.tags.push name
        Post.save args, callbacks.save_sucсess, callbacks.error

      $scope.ui =
        is_ready: true
        saved: false
        changed: false
        is_dialog: false
        music_price: 50
        collapsed: false
        init: ->
          $scope.form =
            tags: $scope.post.tags
            description: $scope.post.description
            price: $scope.post.price
            category: $scope.post.category
        price: (kind) ->
          switch kind
            when 'total'
              return 0 unless $scope.form.price
              $scope.form.price + $scope.ui.music_price
            when 'receive'
              return '0$' unless $scope.form.price
              result = $scope.form.price
              if result < 0
                '-$' + Math.abs(result)
              else
                '$' + result
            else $scope.form.price
        canAction: (kind) ->
          return false unless $scope.ui.is_ready
          return false if $scope.ui.changed
          true
        onChange: -> $scope.ui.saved = false; init_save_timeout()
        onMeans: ->
          if $scope.ui.is_dialog
            $scope.ui.is_dialog = !$scope.ui.is_dialog
            return
          return unless $scope.ui.is_ready
          $scope.ui.is_ready = false
          Post.preview { id: $scope.post.id }, (preview) ->
            [$scope.ui.is_ready, $scope.ui.is_dialog, $scope.data.preview] = [true, true, preview]

      $scope.$watch 'post', (o, n) ->
        if $scope.post.$promise
          $scope.post.$promise.then $scope.ui.init

      $scope.form = {}
      $scope.data = data
  ]