postsFormFirstStepCtrl = ($scope, Post, Category, Packshot) ->
  if $scope.post.$promise
    $scope.post.$promise.then ->
      ui.upload.params.upload_path = $scope.post.ui.upload_video_post_path
  else
    ui.upload.params.upload_path = $scope.post.ui.upload_video_post_path

  ui =
    collapsed: false
    saved: false
    is_uploading: false
    uploading_percent: 0
    upload:
      params:
        upload_path: null
    beforeRequest: ($data, $callback) =>
      [$scope.ui.is_uploading, $scope.ui.saved, $scope.errors] = [false, false, null]
      $data = { duration: 0 } unless $data && typeof $data is 'object'
      if $data.duration < 5 || $data.duration > @settings().video.max_duration + 1
        $scope.errors = { base: ["Error: Your video have incorrect duration, #{$data.duration} instead #{@settings().video.max_duration} seconds! Correct one please and try again."] }
        return
      [$scope.ui.is_uploading, $scope.ui.saved] = [true, false];
      $scope.$emit 'packshots:video:ready'
      $callback()
    onRequestProcess: ($percent_complete) -> $scope.ui.uploading_percent = $percent_complete
    onRequestSuccess: ($data) ->
      [$scope.ui.is_uploading, $scope.ui.saved] = [false, true]
      $scope.refresh { $post: $data }
    onRequestError: ($errors) -> $scope.errors = $errors
    onVideoDelete: ->
      $scope.post.video = null
      $scope.refresh { $post: $scope.post }

  $scope.$on 'posts:cancel', (e, d) -> ui.upload.params.upload_path = d.ui.upload_video_post_path

  $scope.ui = ui

postsFormFirstStepCtrl.prototype = Object.create @BaseDirective.prototype

@app.directive 'postsFormFirstStep', ->
  restrict: 'E'
  transclude: true
  replace: true
  scope:
    post: '='
    data: '='
    refresh: '&'
  templateUrl: (elem, attrs) =>
    if attrs.creative
      @template 'directives/posts/form/creative_first_step'
    else
      @template 'directives/posts/form/first_step'
  controller: ['$scope', 'Post', 'Category', 'Packshot', postsFormFirstStepCtrl]
