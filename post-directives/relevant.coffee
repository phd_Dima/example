@app.directive 'relevant', ->
  restrict: 'E'
  transclude: true
  replace: true
  scope:
    relevant: '='
  templateUrl: @template 'directives/posts/relevant'
  controller: [
    '$scope'
    ($scope) ->
  ]