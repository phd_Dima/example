feedDirectiveCtrl = ($scope, Post) ->
  self = this

  $scope.vjs =
    is_ready: false
    is_poster_ready: false
    options:
      loop: false
      muted: true
      preload: 'metadata'
      autoplay: false
      width: 360
      height: 200
      poster: null
    media:
      sources: [
        { src: null, type: 'application/x-mpegURL' }
      ]
      tracks: []

  $scope.headerClass = ->
    if $scope.profile
      'feeds-header-creative'
    else
      'feeds-header-customer'

  $scope.like = ->
    # for unregistered
    return window.location.replace $scope.feed.ui.sign_in_url if $scope.feed.ui.sign_in_url
    @feed.is_liked = !@feed.is_liked
    Post.concern_like({ id: @feed.id }).$promise.then(
      (response) =>
        @feed.is_liked = response.is_liked
        @feed.likes_count = response.likes_count
    )

  $scope.wish = ->
    if @feed.is_wished == false
      Post.addToWishlist({ id: @feed.id }).$promise.then(
        (response) =>
          @feed.is_wished = true
        )
    else
      Post.removeFromWishlist({ id: @feed.id }).$promise.then(
        (response) =>
          @feed.is_wished = false
          @$emit 'post:diswish', @feed
      )

  $scope.remove = ->
    if confirm 'Are you sure?'
      Post.destroy({ id: @feed.id }).$promise.then(
        (response) =>
          @onRemove { id: @feed.id }
      )

  $scope.$on 'vjsVideoReady', (e, data) ->
    $scope.vjs_player = data.player
    $scope.vjs_player.volume 0 # mute
    $scope.vjs_player.controls false
    $scope.vjs_player.one 'loadedmetadata', ->
      $scope.vjs.is_ready = true
      self.apply $scope

  ui =
    is_ready: false
    likes_visible: false
    init: ->
      $scope.vjs.media.sources[0].src = ($scope.feed.video || {}).preview_hls_url
      $scope.vjs.options.poster = ($scope.feed.video || {}).poster_medium_url
      ui.is_ready = true
    onLikes: -> ui.likes_visible = !ui.likes_visible

  if $scope.feed.$promise
    $scope.feed.$promise.then ui.init
  else
    ui.init()

  $scope.can_buy = $scope.feed.ui.can_purchase
  $scope.sold = $scope.feed.ui.purchased && ($scope.market || !$scope.user.ui.is_business)
  $scope.bought = $scope.feed.ui.bought && $scope.user.ui.is_business
  $scope.can_manage = $scope.feed.ui.can_manage && !$scope.market

  $scope.ui = ui


feedDirectiveCtrl.prototype = Object.create @BaseDirective.prototype

@app.directive 'feed', ->
  restrict: 'E'
  transclude: true
  replace: true
  scope:
    feed: '='
    reply: '='
    user: '='
    profile: '='
    market: '='
    onRemove: '&'
  templateUrl: @template 'directives/posts/feed'
  link: ($scope, element, attrs) ->
    self = angular.element element
    self.find '.feeds-video'
    .on 'mouseenter', (d) ->
        $scope.vjs_player.play() if $scope.vjs_player
    .on 'mouseleave', (d) ->
        $scope.vjs_player.pause() if $scope.vjs_player
  controller: ['$scope', 'Post', feedDirectiveCtrl]
