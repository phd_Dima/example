'use strict'

PostsShowCtrl = ($scope, Post, Comment) ->
  self = this

  data =
    post_id: null
    user: self.currentUser()

  vjs =
    is_ready: false
    paused: true
    is_poster_ready: false
    have_metadata: false
    options:
      loop: false
      muted: false
      preload: 'metadata'
      autoplay: false
      controls: false
      width: 720
      height: 405
      poster: null
    media:
      sources: [{ src: null, type: 'application/x-mpegURL' }]
    play: ->
      return unless vjs.player
      vjs.paused = false
      vjs.player.play()
      vjs.player.controls true
    pause: ->
      return unless vjs.player
      vjs.player.pause()
      vjs.paused = true
    fetch_hls_url: ->
      Post.build_hls_url { id: $scope.post.id, packshot_id: ($scope.post.packshot || {}).id }, (data) ->
        vjs.media.sources[0].src = data.url
        vjs.is_ready = true

  $scope.$on 'vjsVideoReady', (e, data) ->
    return if ui.state
    vjs.player = data.player
    vjs.player.removeChild 'BigPlayButton'
    vjs.player.one 'loadedmetadata', ->
      vjs.have_metadata = true
      self.apply $scope

  ui =
    state: null
    played: false
    onState: (state) ->
      if ui.state is state
        ui.state = null
        vjs.is_ready = true
        return
      ui.state = state
      vjs.is_ready = false
    onPlay: ->
      ui.played = true
      vjs.play()

  $scope.init = (id) ->
    data.post_id = id
    $scope.post = Post.show { id: id }, ->
      $scope.comments_loadable = $scope.post.comments.length < $scope.post.count_comments
      vjs.options.poster = ($scope.post.video || {}).poster
      vjs.fetch_hls_url()
    $scope.reply =
      parent_id: null
      post_id: id
      clear: ->
        @text = ''
      submit: (parent) ->
        Comment.create { comment: $scope.reply }, (comment) =>
          @parent_id = null
          @clear()
          parent.replies_count++
          parent.replies.unshift comment

    $scope.comments_page = 1

    $scope.moreComments = ->
      Comment.index { page: ++@comments_page, post_id: @post.id }, (comments) =>
        @post.comments = @post.comments.concat(comments)
        @comments_loadable = @post.comments.length < @post.count_comments

  $scope.data = data
  $scope.vjs = vjs
  $scope.ui = ui
  self


PostsShowCtrl.prototype = Object.create @BaseDirective.prototype

@app.controller 'PostsShowCtrl', ['$scope', 'Post', 'Comment', PostsShowCtrl]
