'use strict';

PostsNewCtrl = ($scope, ws, packshot, Post, Category) ->
  vm = this

  vm.data =
    errors: null
    categories: []
    packshot:
      errors: null
    user: @currentUser()

  resource_callbacks =
    error: (d) -> vm.data.errors = d.data.errors
    cancel_suсcess: (d) ->
      [vm.ui.is_ready, vm.post] = [true, d]; vm.ui.init()
      $scope.$broadcast 'posts:cancel', d

  ws_callbacks =
    onOpen: -> console.log 'ws.onOpen'
    onMessage: (message) ->
      switch message.kind
        when 'posts:preview:ready'
          vm.post = message.post if vm.post.id is message.post.id
        when 'packshots:video:ready'
          vm.post.packshot = message.packshot
          $scope.$broadcast 'packshots:video:ready', message.packshot
        when 'posts:video:status' then $scope.$broadcast 'posts:video:status', message.job
        else console.log 'Unforeseen message kind', message.kind
    onClose: -> console.log 'ws.onClose'

  vm.post = Post.new {}, ->
    vm.data.categories = Category.query {}, vm.ui.init

  vm.ui =
    is_ready: true
    init: ->
      $scope.$broadcast 'select:init'
    onRefresh: ($post) -> vm.post = $post
    onCancel: ->
      return false unless vm.ui.is_ready && vm.ui.cancel.can()
      vm.ui.is_ready = false
      Post.cancel { id: vm.post.id }, resource_callbacks.cancel_suсcess, resource_callbacks.error
    cancel:
      init: -> vm.ui.cancel.class['btn--inactive'] = !vm.ui.cancel.can()
      can: ->
        return false unless vm.post
        vm.post.video && vm.post.video.file_name || vm.post.packshot && (vm.post.packshot.brand_name || vm.post.packshot.slogan)
      class:
        'btn--inactive': false
    preview:
      init: -> vm.ui.preview.class['btn--inactive'] = !vm.ui.preview.path()
      path: ->
        return null unless packshot.canPreview()
        return null unless vm.post && vm.post.video && vm.post.video.file_name
        _packshot = packshot.models.current() || vm.post.packshot
        vm.post.ui.preview_post_path + '?packshot_id=' + _packshot.id
      class:
        'btn--inactive': false

  watchForActions = ->
    vm.ui.preview.init()
    vm.ui.cancel.init()

  $scope.$on 'packshots:video:ready', ->
    watchForActions()
    $scope.$broadcast 'posts:preview'

  $scope.$watch 'vm.post.video', watchForActions
  $scope.$watch 'vm.post.packshot', watchForActions

  ws.init { user_id: vm.data.user.id }, ws_callbacks

  vm


PostsNewCtrl.prototype = Object.create @BaseCtrl.prototype

angular.module('RootApp').controller 'PostsNewCtrl', ['$scope', 'ws', 'packshot', 'Post', 'Category', PostsNewCtrl]
