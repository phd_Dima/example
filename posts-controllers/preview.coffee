'use strict'

PostsPreviewCtrl = ->
  # It's really simple, but don't underestimate!
  vm = this

  ui =
    withdraw_kind: 'pay_pal'

  vm.ui = ui
  vm


angular.module('RootApp').controller 'PostsPreviewCtrl', [PostsPreviewCtrl]
