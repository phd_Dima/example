'use strict';

CreativePostsNewCtrl = ($scope, ws, packshot, Post, Category) ->
  vm = this

  vm.data =
    errors: null
    categories: []
    packshot:
      errors: null
    user: @currentUser()

  resource_callbacks =
    error: (d) -> vm.data.errors = d.data.errors

  ws_callbacks =
    onOpen: -> console.log 'ws.onOpen'
    onMessage: (message) ->
      switch message.kind
        when 'posts:preview:ready'
          return unless vm.post.id is message.post.id
          vm.post = message.post
        when 'packshots:video:ready'
          vm.post.packshot = message.packshot
          $scope.$broadcast 'packshots:video:ready', message.packshot
        when 'posts:video:status' then $scope.$broadcast 'posts:video:status', message.job
        else console.log 'Unforeseen message kind', message.kind
    onClose: -> console.log 'ws.onClose'

  vm.post = Post.new {}, ->
    vm.data.categories = Category.query {}, vm.ui.init

  ui =
    is_ready: true
    init: ->
      $scope.$broadcast 'select:init'
    onRefresh: ($post) -> vm.post = $post
    canPublish: ->
      return null unless packshot.canPreview()
      return null unless vm.post && vm.post.video && vm.post.video.file_name
      true
    preview:
      init: -> ui.preview.class['btn--inactive'] = !ui.canPublish()
      path: -> '/congratulations' if ui.canPublish()


  $scope.$on 'packshots:video:ready', ->
    $scope.$broadcast 'posts:preview'

  ws.init { user_id: vm.data.user.id }, ws_callbacks

  vm.ui = ui
  vm


CreativePostsNewCtrl.prototype = Object.create @BaseCtrl.prototype

angular.module('RootApp').controller 'CreativePostsNewCtrl', ['$scope', 'ws', 'packshot', 'Post', 'Category', CreativePostsNewCtrl]
