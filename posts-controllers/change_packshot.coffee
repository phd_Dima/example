'use strict'

PostsChangePackshotCtrl = ($scope, ws, packshot, Post, Packshot) ->
  vm = this

  data =
    errors: null
    packshot:
      errors: null
    user: @currentUser()
    back_url: null

  ws_callbacks =
    onOpen: -> console.log 'ws.onOpen'
    onMessage: (message) ->
      switch message.kind
        when 'posts:preview:ready'
          vm.post = message.post if vm.post.id is message.post.id
        when 'packshots:video:ready'
          vm.post.packshot = message.packshot
          $scope.$broadcast 'packshots:video:ready', message.packshot
        when 'posts:video:status' then $scope.$broadcast 'posts:video:status', message.job
        else console.log 'Unforeseen message kind', message.kind
    onClose: -> console.log 'ws.onClose'

  ui =
    is_ready: false
    init: (post_id, packshot_id, back_url) ->
      data.back_url = back_url
      Post.edit { id: post_id }, (post) ->
        vm.post = post
        packshot.models.generate = post.packshot
        packshot.models.upload = post.packshot
        if packshot_id * 1 is (post.packshot || {}).id
          ui.is_ready = true
          $scope.$broadcast 'posts:preview'
          return
        # retrieve selected packshot
        packshot.models.chosen = Packshot.edit { id: packshot_id }, ->
          packshot.state = 'upload'
          ui.is_ready = true
          $scope.$broadcast 'posts:preview'
    onRefresh: ($post) -> vm.post = $post
    canPreview: packshot.canPreview
    onSave: ->
      return unless ui.is_ready && ui.canPreview()
      ui.is_ready = false
      Post.update_packshot { id: vm.post.id, packshot_id: packshot.models.current().id }, ->
        window.location.replace data.back_url

  ws.init { user_id: data.user.id }, ws_callbacks

  $scope.$on 'packshots:video:ready', ->
    $scope.$broadcast 'posts:preview'

  vm.post = null
  vm.data = data
  vm.ui = ui
  vm


PostsChangePackshotCtrl.prototype = Object.create @BaseCtrl.prototype

angular.module('RootApp').controller 'PostsChangePackshotCtrl', ['$scope', 'ws', 'packshot', 'Post', 'Packshot', PostsChangePackshotCtrl]
