'use strict';

PostsEditCtrl = ($scope, ws, Post, Category, Packshot) ->
  vm = this

  vm.data =
    errors: null
    categories: []
    upload_video_url: null
    packshot:
      errors: null
    user: @currentUser()

  resource_callbacks =
    error: (d) -> vm.data.errors = d.data.errors
    cancel_suсcess: (d) -> [vm.ui.is_ready, vm.post] = [true, d]; vm.ui.init()

  ws_callbacks =
    onOpen: -> console.log 'ws.onOpen'
    onMessage: (message) ->
      switch message.kind
        when 'posts:preview:ready' then vm.post = message.post
        when 'packshots:video:ready' then vm.post.packshot = message.packshot
        when 'posts:video:status' then $scope.$broadcast 'posts:video:status', message.job
        else console.log 'Unforeseen message kind', message.kind
    onClose: -> console.log 'ws.onClose'

  $scope.init = (id) ->
    vm.post = Post.edit { id: id }, ->
      vm.post.packshot = {} unless vm.post.packshot
      vm.data.upload_video_url = vm.post.ui.upload_video_post_path
      vm.data.categories = Category.query {}, vm.ui.init

  vm.ui =
    is_ready: true
    init: ->
      vm.post.packshot = {} unless vm.post.packshot
      vm.data.upload_video_url = vm.post.ui.upload_video_post_path
      $scope.$broadcast 'select:init'
    onRefresh: ($post) -> vm.post = $post
    onCancel: ->
      return false unless vm.ui.is_ready && vm.ui.cancel.can()
      vm.ui.is_ready = false
      Post.cancel { id: vm.post.id }, resource_callbacks.cancel_suсcess, resource_callbacks.error
    cancel:
      init: -> vm.ui.cancel.class['btn--inactive'] = !vm.ui.cancel.can()
      can: -> vm.post.video && vm.post.video.file_name || vm.post.packshot && (vm.post.packshot.brand_name || vm.post.packshot.slogan)
      class:
        'btn--inactive': false
    preview:
      init: -> vm.ui.preview.class['btn--inactive'] = !vm.ui.preview.path()
      path: ->
        return null unless vm.post.packshot && vm.post.packshot.brand_name && vm.post.packshot.slogan
        return null unless vm.post.video && vm.post.video.file_name
        vm.post.ui.preview_post_path
      class:
        'btn--inactive': false

  watchForActions = ->
    vm.ui.preview.init()
    vm.ui.cancel.init()

  $scope.$watch 'vm.post.video', watchForActions
  $scope.$watch 'vm.post.packshot', watchForActions

  ws.init { user_id: vm.data.user.id }, ws_callbacks

  vm


PostsEditCtrl.prototype = Object.create @BaseCtrl.prototype

@app.controller 'PostsEditCtrl', ['$scope', 'ws', 'Post', 'Category', 'Packshot', PostsEditCtrl]
